import json
import os
import uuid

import pika


class RPC(object):

    def __init__(self):
        self.response = None
        self.corr_id = None

        self.credentials = pika.PlainCredentials(
            username=os.environ.get('RABBITMQ_USERNAME'),
            password=os.environ.get('RABBITMQ_PASSWORD')
        )

        self.params = pika.ConnectionParameters(
            host=os.environ.get('RABBITMQ_HOST'),
            port=os.environ.get('RABBITMQ_PORT'),
            credentials=self.credentials,
            heartbeat=600,
            blocked_connection_timeout=300
        )

        self.connection = pika.BlockingConnection(self.params)
        self.channel = self.connection.channel()

        result = self.channel.queue_declare(queue='', exclusive=True)
        self.callback_queue = result.method.queue

        self.channel.basic_consume(
            queue=self.callback_queue,
            on_message_callback=self.on_response,
            auto_ack=True
        )

    def on_response(self, ch, method, props, body):
        data = json.loads(body)
        if self.corr_id == props.correlation_id:
            self.response = data

    def call_payment_service(self, method, body):
        self.response = None
        self.corr_id = str(uuid.uuid4())
        self.channel.basic_publish(
            exchange='',
            routing_key='payment_rpc_queue',
            properties=pika.BasicProperties(
                content_type=method,
                reply_to=self.callback_queue,
                correlation_id=self.corr_id,
                priority=7
            ),
            body=json.dumps(body)
        )

        while self.response is None:
            self.connection.process_data_events()

        return self.response
