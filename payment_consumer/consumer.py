import json
import os

import django
import pika

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")
django.setup()

from payments.models import User
from rpc import RPC


def on_rpc_message(channel, method_frame, header_frame, body):
    data = json.loads(body)
    response = {'content_type': 'failure'}

    if header_frame.content_type == 'checkout':
        user = User.objects.get(id=data['user'])

        if user.credit >= data['total_cost']:
            stock_service_rpc = RPC()
            rpc_response = stock_service_rpc.call_stock_service('reduce_stock', data)

            if rpc_response['content_type'] == 'stock_reduced':
                user.credit -= data['total_cost']
                user.save()

                response = {'content_type': 'success'}

    channel.basic_publish(
        exchange='',
        routing_key=header_frame.reply_to,
        properties=pika.BasicProperties(
            priority=10,
            correlation_id= \
                header_frame.correlation_id
        ),
        body=json.dumps(response)
    )


credentials = pika.PlainCredentials(
    username=os.environ.get('RABBITMQ_USERNAME'),
    password=os.environ.get('RABBITMQ_PASSWORD')
)

params = pika.ConnectionParameters(
    host=os.environ.get('RABBITMQ_HOST'),
    port=os.environ.get('RABBITMQ_PORT'),
    credentials=credentials,
    heartbeat=600,
    blocked_connection_timeout=300
)

connection = pika.BlockingConnection(params)

channel = connection.channel()
channel.queue_declare(queue='payment_rpc_queue')
channel.basic_consume(queue='payment_rpc_queue', on_message_callback=on_rpc_message, auto_ack=True)

try:
    print('Started consuming')
    channel.start_consuming()
except KeyboardInterrupt:
    channel.stop_consuming()

connection.close()
