Django==3.2.4
pika==1.2.0
psycopg2-binary==2.8.6
django-cockroachdb==3.2.1
pika-stubs==0.1.3
django-extensions==3.1.3
