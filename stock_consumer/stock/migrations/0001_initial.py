# Generated by Django 3.2.4 on 2021-06-03 07:54

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Item',
            fields=[
                ('id', models.BigAutoField(primary_key=True, serialize=False)),
                ('stock', models.IntegerField(default=0)),
                ('cost', models.FloatField(default=0.0)),
            ],
        ),
    ]
